"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tag_1 = __importDefault(require("graphql-tag"));
exports.GET_CHILD_NODES = graphql_tag_1.default `
  query ChildCompositeEntityNodes($id: ID!)
  {
    ChildCompositeEntityNodes(id:$id) {
      configObjectId
      accessibilityRegex
      order
      expressionAvailable
      expressionFieldString
      entity {
        configObjectId
        dbTypeName
       logicalColumns {
          isPrimaryKey
          dataType
          dbCode
          length
          dbType
          isMandatory
          jsonName
          mode
          isUnique
          configObjectId
          isDerived
          sourceColumn {
           dbCode
      }
          standardValidations {
            configObjectId
            mode
            defaultErrorMessage
            validationType
            regex
            isConditionAvailable
            conditionExpression
          }
          databaseValidations {
            configObjectId
            mode
            datasourceName
            validationType
            validationExpression
            validationQid
            validationMessage
            validationExpressionKeys
            isConditionAvailable
            conditionExpression
          }
        }
        formDbValidations {
          configObjectId
          mode
          datasourceName
          validationType
          validationExpression
          validationQid
          validationMessage
          validationExpressionKeys
          fieldIds
          isConditionAvailable
          conditionExpression
        }
      }
      physicalDataEntities {
        configObjectId
        compositeEntityId
        expressionAvailable
        accessibilityRegex
        isMultivalueMapping
        order
        dbType
        dbTypeName
        schemaName
        workAreaSessName
        insertQID
        updateQID
        sequenceQID
        singleSelectQID
        multiSelectQID
        deleteQID
        isPrimaryEntity
       physicalColumns {
          isKey
          dbCode
          length
          isDisplayColumn
          isPrimaryKey
          dataType
          dbType
          jsonName
          isUnique
          isVirtualColumn
          isMultiValueField
          configObjectId
          isPhysicalColumnMandatory
         }
      }
      nodeBusinessRules {
        configObjectId
        executionType
        rule
        order
        name
      }
    }
}`;
//# sourceMappingURL=child.nodes.js.map