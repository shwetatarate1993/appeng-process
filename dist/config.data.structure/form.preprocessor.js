"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tag_1 = __importDefault(require("graphql-tag"));
exports.FORM_PREPROCESSORS = graphql_tag_1.default `
  query Form($id: ID!)
  {
    Form(id:$id)
  {
    configObjectId
    columnDataPreprocessors {
      configObjectId
      name
      excecutionType
      jsCode
      order
    }
  }
}`;
//# sourceMappingURL=form.preprocessor.js.map