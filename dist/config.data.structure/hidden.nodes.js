"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tag_1 = __importDefault(require("graphql-tag"));
exports.GET_HIDDEN_NODES = graphql_tag_1.default `
  query HiddenNodes($id: ID!)
  {
    HiddenNodes(id:$id) {
      configObjectId
      order
      entity {
        configObjectId
        dbTypeName
      }
      physicalDataEntities {
        configObjectId
        expressionAvailable
        accessibilityRegex
        isMultivalueMapping
        order
        dbType
        dbTypeName
        schemaName
        workAreaSessName
        insertQID
        updateQID
        sequenceQID
        singleSelectQID
        multiSelectQID
        deleteQID
        isPrimaryEntity
       physicalColumns {
          isKey
          dbCode
          length
          isDisplayColumn
          isPrimaryKey
          dataType
          dbType
          jsonName
          isUnique
          isVirtualColumn
          isMultiValueField
          configObjectId
          isPhysicalColumnMandatory
         }
      }
    }
}`;
//# sourceMappingURL=hidden.nodes.js.map