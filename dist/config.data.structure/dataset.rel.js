"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tag_1 = __importDefault(require("graphql-tag"));
exports.GET_DATASET_REL = graphql_tag_1.default `
  query DataSetRelEntityColumn($parentEntityId: ID!,$childEntityId: ID!)
  {
  DataSetRelEntityColumn(parentEntityId:$parentEntityId,childEntityId:$childEntityId)
  {
   dataSetRelColumns{
      parentColumn {
        dbCode
      }
      childColumn {
        dbCode
      }
    }
  }
}`;
//# sourceMappingURL=dataset.rel.js.map