"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const mockdata_1 = require("../../mockdata");
const services_1 = require("../../services");
init_config_1.AppengProcessConfig.configure(config_1.default.get(constant_1.DB), config_1.default.get(constant_1.LINKS), 'sqlite3');
test('inbound process', async () => {
    const servicesMock = init_config_1.AppengProcessConfig.INSTANCE.serviceOrchestrator;
    const mocked = sinon_1.default.mock(servicesMock);
    mocked.expects('selectSingleRecord').atLeast(1).returns({});
    mocked.expects('selectRecords').never().returns([]);
    mocked.expects('createSequence').atLeast(1).returns({ ID: 1 });
    mocked.expects('insert').atLeast(1).returns(1);
    const inboundProcess = new services_1.InboundProcess(servicesMock);
    const logicalGroupData = JSON.parse(JSON.stringify(mockdata_1.entityGroupDataMock));
    const result = await inboundProcess.process(logicalGroupData);
    mocked.verify();
    sinon_1.default.assert.match(result.pk, 1);
    console.log('result...', result);
    mocked.restore();
});
//# sourceMappingURL=inbound.process.test.js.map