import { ServiceOrchestrator } from 'appeng-db';
import { EntityGroupData } from '../models';
export declare class CoreProcess {
    coreProcessExecute(entityGroupData: EntityGroupData, serviceOrchestrator: ServiceOrchestrator): Promise<any>;
}
declare const coreProcess: CoreProcess;
export default coreProcess;
//# sourceMappingURL=core.process.service.d.ts.map