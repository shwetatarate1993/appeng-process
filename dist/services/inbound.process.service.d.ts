import { ServiceOrchestrator } from 'appeng-db';
import { EntityGroupData } from '../models';
export declare class InboundProcess {
    serviceOrchestrator: ServiceOrchestrator;
    constructor(serviceOrchestrator: ServiceOrchestrator);
    process(entityGroupData: EntityGroupData): Promise<any>;
}
declare const createInboundProcess: (serviceOrchestrator: ServiceOrchestrator) => InboundProcess;
export default createInboundProcess;
//# sourceMappingURL=inbound.process.service.d.ts.map