import { ServiceOrchestrator } from 'appeng-db';
export declare class DataPreProcessor {
    serviceOrchestrator: ServiceOrchestrator;
    constructor(serviceOrchestrator: ServiceOrchestrator);
    process(formData: any, configId: string, configType: string): Promise<any>;
    getDataPreprocessors(configId: string, query: string): Promise<any>;
}
declare const createDataPreProcessor: (serviceOrchestrator: ServiceOrchestrator) => DataPreProcessor;
export default createDataPreProcessor;
//# sourceMappingURL=column.data.preprocessor.service.d.ts.map