"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_flow_1 = require("appeng-flow");
const config_data_structure_1 = require("../config.data.structure");
const constant_1 = require("../constant");
const init_config_1 = require("../init-config");
const mockdata_1 = require("../mockdata");
const info_commons_1 = require("info-commons");
const appeng_meta_1 = __importDefault(require("appeng-meta"));
const metadata_utils_1 = require("../utils/metadata.utils");
let client;
class DataPreProcessor {
    constructor(serviceOrchestrator) {
        this.serviceOrchestrator = serviceOrchestrator;
    }
    async process(formData, configId, configType) {
        if (info_commons_1.APP_ENV === 'server') {
            client = metadata_utils_1.createApolloClient(init_config_1.AppengProcessConfig.appengMetaURI);
        }
        let dataPreprocessors = [];
        let data = {};
        switch (configType) {
            case constant_1.ConfigType.FORMFIELD:
                //if (process.env.NODE_ENV === 'test') {
                    //data = JSON.parse(JSON.stringify(mockdata_1.dataPreprocessorMock));
                //}
               // else {
                    data = await this.getDataPreprocessors(configId, config_data_structure_1.FORMFILED_PREPROCESSORS);
                //}
                dataPreprocessors = data.FormField.columnDataPreprocessors;
                break;
            case constant_1.ConfigType.FORM:
                data = await this.getDataPreprocessors(configId, config_data_structure_1.FORM_PREPROCESSORS);
                dataPreprocessors = data.Form.columnDataPreprocessors;
                break;
        }
        formData = await appeng_flow_1.dataPreprocessorsExecutor(dataPreprocessors, formData, this.serviceOrchestrator);
        return formData;
    }
    async getDataPreprocessors(configId, query) {
        try {
            if (info_commons_1.APP_ENV === 'desktop') {
                query = metadata_utils_1.manipulateQuery(query);
                query = query.replace("$id", '"' + configId + '"');
                const { data } = await appeng_meta_1.default(query);
                return data;
            }
            else {
                const { data } = await client.query({
                    query,
                    variables: { id: configId },
                });
                return data;
            }
        }
        catch (error) {
            throw error;
        }
    }
}
exports.DataPreProcessor = DataPreProcessor;
let dataPreProcessor;
const createDataPreProcessor = (serviceOrchestrator) => {
    if (!dataPreProcessor) {
        dataPreProcessor = new DataPreProcessor(serviceOrchestrator);
        Object.freeze(dataPreProcessor);
    }
    return dataPreProcessor;
};
exports.default = createDataPreProcessor;
//# sourceMappingURL=column.data.preprocessor.service.js.map