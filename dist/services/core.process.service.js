"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const process_logical_data_1 = require("../parsers/process.logical.data");
const process_physical_data_1 = require("../parsers/process.physical.data");
const transform_to_physical_data_1 = require("../parsers/transform.to.physical.data");
class CoreProcess {
    async coreProcessExecute(entityGroupData, serviceOrchestrator) {
        try {
            const logicalGroupData = await process_logical_data_1.processLogicalData(entityGroupData, serviceOrchestrator);
            const parentPhysicalEntities = logicalGroupData.logicalData.ceNode.physicalDataEntities;
            let masterPhysicalEntity = parentPhysicalEntities[0];
            if (parentPhysicalEntities.length > 1) {
                const parentPE = parentPhysicalEntities.find((pe) => pe.isPrimaryEntity);
                if (parentPE) {
                    masterPhysicalEntity = parentPE;
                }
            }
            const physicalDatas = await transform_to_physical_data_1.transformToPhysicalData(logicalGroupData, serviceOrchestrator);
            const response = await process_physical_data_1.processPhysicalData(physicalDatas, serviceOrchestrator, masterPhysicalEntity.configObjectId);
            return response;
        }
        catch (error) {
            throw error;
        }
    }
}
exports.CoreProcess = CoreProcess;
const coreProcess = new CoreProcess();
Object.freeze(coreProcess);
exports.default = coreProcess;
//# sourceMappingURL=core.process.service.js.map