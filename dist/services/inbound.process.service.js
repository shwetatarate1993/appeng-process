"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const services_1 = require("./../services");
class InboundProcess {
    constructor(serviceOrchestrator) {
        this.serviceOrchestrator = serviceOrchestrator;
    }
    async process(entityGroupData) {
        try {
            await services_1.preProcess.preprocessor();
            const response = await services_1.coreProcess
                .coreProcessExecute(entityGroupData, this.serviceOrchestrator);
            await services_1.postProcess.postprocessor();
            return response;
        }
        catch (error) {
            throw error;
        }
    }
}
exports.InboundProcess = InboundProcess;
let inboundProcess;
const createInboundProcess = (serviceOrchestrator) => {
    if (inboundProcess === undefined || inboundProcess === null) {
        inboundProcess = new InboundProcess(serviceOrchestrator);
        Object.freeze(inboundProcess);
    }
    return inboundProcess;
};
exports.default = createInboundProcess;
//# sourceMappingURL=inbound.process.service.js.map