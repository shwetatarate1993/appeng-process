"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    dataSetRelColumns: [
        {
            childColumn: {
                dbCode: 'COMPANY_ID',
                __typename: '',
            },
            parentColumn: {
                dbCode: 'ID',
                __typename: '',
            },
        },
        {
            childColumn: {
                dbCode: 'EMPLOYEE_ID',
                __typename: '',
            },
            parentColumn: {
                dbCode: 'ID',
                __typename: '',
            },
        },
        {
            childColumn: {
                dbCode: 'CHECKLIST_ID',
                __typename: '',
            },
            parentColumn: {
                dbCode: 'CHECKLIST_ID',
                __typename: '',
            },
        },
    ],
};
//# sourceMappingURL=dataset.rel.js.map