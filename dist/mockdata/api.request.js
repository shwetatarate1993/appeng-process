"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    action: 'Save',
    method: null,
    baseEntity: {
        name: 'en-!lAnGuAge!-Year-!sEpArAToR!-gu-!lAnGuAge!-વર્ષ',
        configId: '3da282f4-aef2-4154-9790-09da03f119f3',
        nodeId: 'cac1370e-f97e-4401-818c-1f65727ed0a3',
        records: [
            {
                APP_LOGGED_IN_SUB_PROJECT_ID: '1',
                APP_LOGGED_IN_YEAR: '1',
                APP_LOGGED_IN_USER_DISTRICT: '940803e9-56a9-11e9-b039-0ee72c6ddce6',
                APP_CONFIGURATION_PROJECT_ID: '1',
                APP_CURRENT_DATE: '2019-05-28',
                APP_LOGGED_IN_USER_STATE: 'e8a18ea2-56a7-11e9-b039-0ee72c6ddce6',
                APP_LOGGED_IN_USER_GP: 'null',
                APP_LOGGED_IN_USER_TALUKA: 'null',
                APP_LOGGED_IN_YEAR_NAME: '2017-2018',
                APP_LOGGED_IN_USER_ID: '1137',
                isProcessed: true,
                isRenderingRequired: 'false',
                NAME: 'InfoOrigin Tech',
                isDirty: 'true',
                ID: 1,
                CURR_DATE: 1559808039258,
                TITLE: 'InfoOrigin',
            },
        ],
        lePrimaryKey: null,
        childEntities: [
            {
                name: 'en-!lAnGuAge!-Year-!sEpArAToR!-gu-!lAnGuAge!-વર્ષ',
                configId: '3da282f4-aef2-4154-9790-09da03f119f3',
                nodeId: 'cac1370e-f97e-4401-818c-1f65727e',
                records: [
                    {
                        APP_LOGGED_IN_SUB_PROJECT_ID: '1',
                        APP_LOGGED_IN_YEAR: '1',
                        APP_LOGGED_IN_USER_DISTRICT: '940803e9-56a9-11e9-b039-0ee72c6ddce6',
                        APP_CONFIGURATION_PROJECT_ID: '1',
                        APP_CURRENT_DATE: '2019-05-28',
                        APP_LOGGED_IN_USER_STATE: 'e8a18ea2-56a7-11e9-b039-0ee72c6ddce6',
                        APP_LOGGED_IN_USER_GP: 'null',
                        APP_LOGGED_IN_USER_TALUKA: 'null',
                        APP_LOGGED_IN_YEAR_NAME: '2017-2018',
                        APP_LOGGED_IN_USER_ID: '1137',
                        isProcessed: true,
                        isRenderingRequired: 'false',
                        NAME: '2019',
                        isDirty: 'true',
                        ID: 1,
                    },
                ],
                lePrimaryKey: null,
                childEntities: [
                    {
                        name: 'en-!lAnGuAge!-Year-!sEpArAToR!-gu-!lAnGuAge!-વર્ષ',
                        configId: '3da282f4-aef2-4154-9790-09da03f119f3',
                        nodeId: 'cac1370e-f97e-4401',
                        records: [
                            {
                                APP_LOGGED_IN_SUB_PROJECT_ID: '1',
                                APP_LOGGED_IN_YEAR: '1',
                                APP_LOGGED_IN_USER_DISTRICT: '940803e9-56a9-11e9-b039-0ee72c6ddce6',
                                APP_CONFIGURATION_PROJECT_ID: '1',
                                APP_CURRENT_DATE: '2019-05-28',
                                APP_LOGGED_IN_USER_STATE: 'e8a18ea2-56a7-11e9-b039-0ee72c6ddce6',
                                APP_LOGGED_IN_USER_GP: 'null',
                                APP_LOGGED_IN_USER_TALUKA: 'null',
                                APP_LOGGED_IN_YEAR_NAME: '2017-2018',
                                APP_LOGGED_IN_USER_ID: '1137',
                                isProcessed: true,
                                isRenderingRequired: 'false',
                                ADDRESS: 'Adarsh Colony',
                                isDirty: 'true',
                            },
                        ],
                        lePrimaryKey: null,
                        childEntities: [],
                    },
                ],
            }
        ],
    },
};
//# sourceMappingURL=api.request.js.map