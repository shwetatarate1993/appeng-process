declare const _default: {
    FormField: {
        columnDataPreprocessors: {
            configObjectId: string;
            name: string;
            configObjectType: string;
            preProcessorBean: any;
            isMultiValue: boolean;
            excecutionType: string;
            jsCode: string;
            createdBy: string;
            isDeleted: number;
            itemDescription: any;
            creationDate: string;
            projectId: number;
            updatedBy: any;
            updationDate: any;
            deletionDate: any;
            order: any;
        }[];
    };
};
export default _default;
//# sourceMappingURL=formfield.data.preprocessor.d.ts.map