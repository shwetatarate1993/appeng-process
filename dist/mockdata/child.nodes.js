"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'cac1370e-f97e-4401-818c-1f65727e',
        entity: {
            configObjectId: '3da282f4-aef2-4154-9790-09da03f3',
            name: 'Employee',
            configObjectType: 'LogicalEntity',
            createdBy: null,
            isDeleted: null,
            itemDescription: null,
            creationDate: null,
            projectId: null,
            updatedBy: null,
            updationDate: null,
            deletionDate: null,
            dbTypeName: null,
            supportedFlavor: null,
            generateSkeleton: null,
            logicalColumns: [
                {
                    configObjectId: null,
                    name: null,
                    configObjectType: null,
                    createdBy: null,
                    isDeleted: null,
                    itemDescription: null,
                    creationDate: null,
                    projectId: null,
                    updatedBy: null,
                    updationDate: null,
                    deletionDate: null,
                    dataType: null,
                    length: 45,
                    dbCode: 'ID',
                    dbtypeName: null,
                    isPrimaryKey: true,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    jsonName: null,
                },
                {
                    configObjectId: null,
                    name: null,
                    configObjectType: null,
                    createdBy: null,
                    isDeleted: null,
                    itemDescription: null,
                    creationDate: null,
                    projectId: null,
                    updatedBy: null,
                    updationDate: null,
                    deletionDate: null,
                    dataType: null,
                    length: 45,
                    dbCode: 'COMPANY_ID',
                    dbtypeName: null,
                    isPrimaryKey: false,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    jsonName: null,
                },
                {
                    configObjectId: null,
                    name: null,
                    configObjectType: null,
                    createdBy: null,
                    isDeleted: null,
                    itemDescription: null,
                    creationDate: null,
                    projectId: null,
                    updatedBy: null,
                    updationDate: null,
                    deletionDate: null,
                    dataType: null,
                    length: 45,
                    dbCode: 'NAME',
                    dbtypeName: null,
                    isPrimaryKey: false,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    jsonName: 'NAME',
                },
            ],
        },
        physicalDataEntities: [
            {
                configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                name: 'PhysicalEntity',
                configObjectType: 'PhysicalEntity',
                createdBy: '1111',
                isDeleted: 0,
                itemDescription: 'PhysicalEntity',
                creationDate: null,
                projectId: 0,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
                schemaName: 'dummy',
                dbtypeName: 'Employee',
                dbtype: 'Table',
                isVerticalEntity: false,
                order: 10,
                singleSelectQID: 'Select ID,NAME,COMPANY_ID FROM Employee where ID =:ID',
                multiSelectQID: 'mysql-!lAnGuAge!-Select ID,NAME,COMPANY_ID FROM'
                    + 'EmployeesEpArAToRsqlite3-!lAnGuAge!-Select ID,NAME,COMPANY_ID FROM Employee',
                updateQID: 'Update Employee set NAME =:NAME where ID =:ID',
                deleteQID: 'Delete from Employee where ID =:ID',
                insertQID: 'INSERT INTO Employee(ID,NAME,COMPANY_ID) VALUES(:ID,:NAME,:COMPANY_ID)',
                sequenceQID: 'Select COALESCE(max(ID),0)+1 as ID from Employee',
                workAreaSessName: 'mock',
                releaseAreaSqlSessionfactoryName: '',
                datasourceType: 'RDBMS',
                expressionAvailable: false,
                accessibilityRegex: null,
                isMultivalueMapping: false,
                physicalColumns: [
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'PhysicalEntity',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'PhysicalEntity',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'number',
                        length: 40,
                        dbCode: 'ID',
                        dbtypeName: 'ID',
                        isPrimaryKey: true,
                        isVirtual: false,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'NAME',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'NAME',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'string',
                        length: 40,
                        dbCode: 'NAME',
                        dbtypeName: 'NAME',
                        isPrimaryKey: false,
                        isVirtual: false,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'PhysicalEntity',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'PhysicalEntity',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'string',
                        length: 40,
                        dbCode: 'COMPANY_ID',
                        dbtypeName: 'COMPANY_ID',
                        isPrimaryKey: false,
                        isVirtual: false,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'PhysicalEntity',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'PhysicalEntity',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'number',
                        length: 40,
                        dbCode: 'Action',
                        dbtypeName: 'Action',
                        isPrimaryKey: false,
                        isVirtual: true,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                ],
            },
        ],
    },
    {
        configObjectId: 'cac1370e-f97e-4401',
        entity: {
            configObjectId: '3da282f4-aef2-4154-9790-09da03f3',
            name: 'Employee Address',
            configObjectType: 'LogicalEntity',
            createdBy: null,
            isDeleted: null,
            itemDescription: null,
            creationDate: null,
            projectId: null,
            updatedBy: null,
            updationDate: null,
            deletionDate: null,
            dbTypeName: null,
            supportedFlavor: null,
            generateSkeleton: null,
            logicalColumns: [
                {
                    configObjectId: null,
                    name: null,
                    configObjectType: null,
                    createdBy: null,
                    isDeleted: null,
                    itemDescription: null,
                    creationDate: null,
                    projectId: null,
                    updatedBy: null,
                    updationDate: null,
                    deletionDate: null,
                    dataType: null,
                    length: 45,
                    dbCode: 'ID',
                    dbtypeName: null,
                    isPrimaryKey: true,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    jsonName: null,
                },
                {
                    configObjectId: null,
                    name: null,
                    configObjectType: null,
                    createdBy: null,
                    isDeleted: null,
                    itemDescription: null,
                    creationDate: null,
                    projectId: null,
                    updatedBy: null,
                    updationDate: null,
                    deletionDate: null,
                    dataType: null,
                    length: 45,
                    dbCode: 'EMPLOYEE_ID',
                    dbtypeName: null,
                    isPrimaryKey: false,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    jsonName: null,
                },
                {
                    configObjectId: null,
                    name: null,
                    configObjectType: null,
                    createdBy: null,
                    isDeleted: null,
                    itemDescription: null,
                    creationDate: null,
                    projectId: null,
                    updatedBy: null,
                    updationDate: null,
                    deletionDate: null,
                    dataType: null,
                    length: 45,
                    dbCode: 'ADDRESS',
                    dbtypeName: null,
                    isPrimaryKey: false,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    jsonName: 'ADDRESS',
                },
            ],
        },
        physicalDataEntities: [
            {
                configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                name: 'PhysicalEntity',
                configObjectType: 'PhysicalEntity',
                createdBy: '1111',
                isDeleted: 0,
                itemDescription: 'PhysicalEntity',
                creationDate: null,
                projectId: 0,
                updatedBy: '1111',
                updationDate: null,
                deletionDate: null,
                schemaName: 'dummy',
                dbtypeName: 'Employee Adress',
                dbtype: 'Table',
                isVerticalEntity: false,
                order: 10,
                singleSelectQID: 'Select ID,EMPLOYEE_ID,ADDRESS FROM Employee_Address where ID =:ID',
                multiSelectQID: 'mysql-!lAnGuAge!-Select ID,EMPLOYEE_ID,ADDRESS FROM'
                    + 'Employee_AddresssEpArAToRsqlite3-!lAnGuAge!-Select ID,'
                    + 'EMPLOYEE_ID,ADDRESS FROM Employee_Address',
                updateQID: 'Update Employee_Address set ADDRESS=:ADDRESS,EMPLOYEE_ID = :EMPLOYEE_ID where ID =:ID',
                deleteQID: 'Delete from Employee_Address where ID =:ID',
                insertQID: 'INSERT INTO Employee_Address(ID,EMPLOYEE_ID,ADDRESS) VALUES(:ID,:EMPLOYEE_ID,:ADDRESS)',
                sequenceQID: 'Select COALESCE(max(ID),0)+1 as ID from Employee_Address',
                workAreaSessName: 'mock',
                releaseAreaSqlSessionfactoryName: '',
                datasourceType: 'RDBMS',
                expressionAvailable: false,
                accessibilityRegex: null,
                isMultivalueMapping: false,
                physicalColumns: [
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'PhysicalEntity',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'PhysicalEntity',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'number',
                        length: 40,
                        dbCode: 'ID',
                        dbtypeName: 'ID',
                        isPrimaryKey: true,
                        isVirtual: false,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'EMPLOYEE_ID',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'EMPLOYEE_ID',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'string',
                        length: 40,
                        dbCode: 'EMPLOYEE_ID',
                        dbtypeName: 'EMPLOYEE_ID',
                        isPrimaryKey: false,
                        isVirtual: false,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'PhysicalEntity',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'PhysicalEntity',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'string',
                        length: 40,
                        dbCode: 'ADDRESS',
                        dbtypeName: 'ADDRESS',
                        isPrimaryKey: false,
                        isVirtual: false,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                    {
                        configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                        name: 'PhysicalEntity',
                        configObjectType: 'PhysicalColumn',
                        createdBy: '1111',
                        isDeleted: 0,
                        itemDescription: 'PhysicalEntity',
                        creationDate: null,
                        projectId: 0,
                        updatedBy: '1111',
                        updationDate: null,
                        deletionDate: null,
                        dataType: 'number',
                        length: 40,
                        dbCode: 'Action',
                        dbtypeName: 'Action',
                        isPrimaryKey: false,
                        isVirtual: true,
                        isKey: false,
                        isPhysicalColumnMandatory: false,
                        isVerticalField: false,
                        isUnique: false,
                        isMandatory: false,
                        isMultivalueField: false,
                    },
                ],
            },
        ],
    },
];
//# sourceMappingURL=child.nodes.js.map