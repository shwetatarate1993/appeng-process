declare const _default: {
    action: string;
    method: any;
    baseEntity: {
        name: string;
        configId: string;
        nodeId: string;
        records: {
            APP_LOGGED_IN_SUB_PROJECT_ID: string;
            APP_LOGGED_IN_YEAR: string;
            APP_LOGGED_IN_USER_DISTRICT: string;
            APP_CONFIGURATION_PROJECT_ID: string;
            APP_CURRENT_DATE: string;
            APP_LOGGED_IN_USER_STATE: string;
            APP_LOGGED_IN_USER_GP: string;
            APP_LOGGED_IN_USER_TALUKA: string;
            APP_LOGGED_IN_YEAR_NAME: string;
            APP_LOGGED_IN_USER_ID: string;
            isProcessed: boolean;
            isRenderingRequired: string;
            NAME: string;
            isDirty: string;
            ID: number;
            CURR_DATE: number;
            TITLE: string;
        }[];
        lePrimaryKey: any;
        childEntities: {
            name: string;
            configId: string;
            nodeId: string;
            records: {
                APP_LOGGED_IN_SUB_PROJECT_ID: string;
                APP_LOGGED_IN_YEAR: string;
                APP_LOGGED_IN_USER_DISTRICT: string;
                APP_CONFIGURATION_PROJECT_ID: string;
                APP_CURRENT_DATE: string;
                APP_LOGGED_IN_USER_STATE: string;
                APP_LOGGED_IN_USER_GP: string;
                APP_LOGGED_IN_USER_TALUKA: string;
                APP_LOGGED_IN_YEAR_NAME: string;
                APP_LOGGED_IN_USER_ID: string;
                isProcessed: boolean;
                isRenderingRequired: string;
                NAME: string;
                isDirty: string;
                ID: number;
            }[];
            lePrimaryKey: any;
            childEntities: {
                name: string;
                configId: string;
                nodeId: string;
                records: {
                    APP_LOGGED_IN_SUB_PROJECT_ID: string;
                    APP_LOGGED_IN_YEAR: string;
                    APP_LOGGED_IN_USER_DISTRICT: string;
                    APP_CONFIGURATION_PROJECT_ID: string;
                    APP_CURRENT_DATE: string;
                    APP_LOGGED_IN_USER_STATE: string;
                    APP_LOGGED_IN_USER_GP: string;
                    APP_LOGGED_IN_USER_TALUKA: string;
                    APP_LOGGED_IN_YEAR_NAME: string;
                    APP_LOGGED_IN_USER_ID: string;
                    isProcessed: boolean;
                    isRenderingRequired: string;
                    ADDRESS: string;
                    isDirty: string;
                }[];
                lePrimaryKey: any;
                childEntities: any[];
            }[];
        }[];
    };
};
export default _default;
//# sourceMappingURL=api.request.d.ts.map