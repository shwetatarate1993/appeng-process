"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    configObjectId: '',
    entity: {
        configObjectId: '3da282f4-aef2-4154-9790-09da03f119f3',
        name: 'Company',
        configObjectType: 'LogicalEntity',
        createdBy: null,
        isDeleted: null,
        itemDescription: null,
        creationDate: null,
        projectId: null,
        updatedBy: null,
        updationDate: null,
        deletionDate: null,
        dbTypeName: 'Company',
        supportedFlavor: null,
        generateSkeleton: null,
        logicalColumns: [
            {
                configObjectId: null,
                name: null,
                configObjectType: null,
                createdBy: null,
                isDeleted: null,
                itemDescription: null,
                creationDate: null,
                projectId: null,
                updatedBy: null,
                updationDate: null,
                deletionDate: null,
                dataType: 'NUMBER',
                length: 45,
                dbCode: 'ID',
                dbtypeName: null,
                isPrimaryKey: true,
                isVirtual: false,
                isKey: false,
                isPhysicalColumnMandatory: false,
                isVerticalField: false,
                isUnique: false,
                isMandatory: false,
                jsonName: null,
            },
            {
                configObjectId: null,
                name: null,
                configObjectType: null,
                createdBy: null,
                isDeleted: null,
                itemDescription: null,
                creationDate: null,
                projectId: null,
                updatedBy: null,
                updationDate: null,
                deletionDate: null,
                dataType: 'VARCHAR',
                length: 45,
                dbCode: 'NAME',
                dbtypeName: null,
                isPrimaryKey: false,
                isVirtual: false,
                isKey: false,
                isPhysicalColumnMandatory: false,
                isVerticalField: false,
                isUnique: false,
                isMandatory: false,
                jsonName: 'NAME',
                mode: 'BOTH',
                standardValidations: [],
                databaseValidations: [
                    {
                        configObjectId: null,
                        name: 'Unique Name',
                        configObjectType: null,
                        createdBy: null,
                        isDeleted: 0,
                        itemDescription: null,
                        creationDate: null,
                        projectId: 0,
                        updatedBy: null,
                        updationDate: null,
                        deletionDate: null,
                        datasourceName: 'mock',
                        datasourceType: 'RDBMS',
                        validationType: 'warning',
                        validationExpression: '#{EXISTING_NAME_COUNT == 0}',
                        validationQid: 'SELECT COUNT(*) AS EXISTING_NAME_COUNT FROM Company where UPPER(NAME) = UPPER(:NAME) and ID!=COALESCE(:ID,0)',
                        validationMessage: 'Name Already Exist',
                        validationExpressionKeys: 'EXISTING_NAME_COUNT',
                    }
                ],
            },
            {
                configObjectId: null,
                name: null,
                configObjectType: null,
                createdBy: null,
                isDeleted: null,
                itemDescription: null,
                creationDate: null,
                projectId: null,
                updatedBy: null,
                updationDate: null,
                deletionDate: null,
                dataType: 'VARCHAR',
                length: 45,
                dbCode: 'PLACE',
                dbtypeName: null,
                isPrimaryKey: false,
                isVirtual: false,
                isKey: false,
                isPhysicalColumnMandatory: false,
                isVerticalField: false,
                isUnique: false,
                isMandatory: false,
                jsonName: 'LOCATION',
            },
            {
                configObjectId: null,
                name: null,
                configObjectType: null,
                createdBy: null,
                isDeleted: null,
                itemDescription: null,
                creationDate: null,
                projectId: null,
                updatedBy: null,
                updationDate: null,
                deletionDate: null,
                dataType: 'VARCHAR',
                length: 45,
                dbCode: 'COMPANY_TITLE',
                dbtypeName: null,
                isPrimaryKey: false,
                isVirtual: false,
                isKey: false,
                isPhysicalColumnMandatory: false,
                isVerticalField: false,
                isUnique: false,
                isMandatory: false,
                jsonName: 'TITLE',
            },
            {
                configObjectId: null,
                name: null,
                configObjectType: null,
                createdBy: null,
                isDeleted: null,
                itemDescription: null,
                creationDate: null,
                projectId: null,
                updatedBy: null,
                updationDate: null,
                deletionDate: null,
                dataType: 'TIMESTAMP',
                length: 45,
                dbCode: 'CURR_DATE',
                dbtypeName: null,
                isPrimaryKey: false,
                isVirtual: false,
                isKey: false,
                isPhysicalColumnMandatory: false,
                isVerticalField: false,
                isUnique: false,
                isMandatory: false,
                jsonName: 'CURR_DATE',
            },
        ],
    },
    physicalDataEntities: [
        {
            configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
            name: 'Company',
            configObjectType: 'PhysicalEntity',
            createdBy: '1111',
            isDeleted: 0,
            itemDescription: 'PhysicalEntity',
            creationDate: null,
            projectId: 0,
            updatedBy: '1111',
            updationDate: null,
            deletionDate: null,
            schemaName: 'dummy',
            dbtypeName: 'Company',
            dbtype: 'Table',
            isVerticalEntity: false,
            order: 10,
            singleSelectQID: 'Select ID,NAME,PLACE,CITY,COUNTRY,EXPENSE FROM Company where ID =:ID',
            multiSelectQID: 'mysql!lAnGuAge!Select ID,NAME,PLACE,CITY,COUNTRY,EXPENSE FROM'
                + 'CompanysEpArAToRsqlite3!lAnGuAge!Select ID,NAME,PLACE,CITY,COUNTRY FROM Company',
            updateQID: 'Update Company set NAME = :NAME,PLACE = :PLACE,CURR_DATE=:CURR_DATE,EXPENSE=:EXPENSE where ID =:ID',
            deleteQID: 'Delete from Company where ID =:ID',
            insertQID: 'INSERT INTO Company(ID,NAME,PLACE,CITY,'
                + 'CURR_DATE,EXPENSE) VALUES(:ID,:NAME,:PLACE,:CITY,:CURR_DATE,:EXPENSE)',
            sequenceQID: 'Select COALESCE(max(ID),0)+1 as ID from Company',
            workAreaSessName: 'mock',
            releaseAreaSqlSessionfactoryName: '',
            datasourceType: 'RDBMS',
            expressionAvailable: false,
            accessibilityRegex: null,
            isMultivalueMapping: false,
            physicalColumns: [
                {
                    configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                    name: 'PhysicalColumn',
                    configObjectType: 'PhysicalColumn',
                    createdBy: '1111',
                    isDeleted: 0,
                    itemDescription: 'PhysicalColumn',
                    creationDate: null,
                    projectId: 0,
                    updatedBy: '1111',
                    updationDate: null,
                    deletionDate: null,
                    dataType: 'NUMBER',
                    length: 40,
                    dbCode: 'ID',
                    dbtypeName: 'ID',
                    isPrimaryKey: true,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    isMultivalueField: false,
                },
                {
                    configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                    name: 'NAME',
                    configObjectType: 'PhysicalColumn',
                    createdBy: '1111',
                    isDeleted: 0,
                    itemDescription: 'NAME',
                    creationDate: null,
                    projectId: 0,
                    updatedBy: '1111',
                    updationDate: null,
                    deletionDate: null,
                    dataType: 'string',
                    length: 40,
                    dbCode: 'NAME',
                    dbtypeName: 'NAME',
                    isPrimaryKey: false,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    isMultivalueField: false,
                },
                {
                    configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                    name: 'PhysicalColumn',
                    configObjectType: 'PhysicalColumn',
                    createdBy: '1111',
                    isDeleted: 0,
                    itemDescription: 'PhysicalColumn',
                    creationDate: null,
                    projectId: 0,
                    updatedBy: '1111',
                    updationDate: null,
                    deletionDate: null,
                    dataType: 'string',
                    length: 40,
                    dbCode: 'PLACE',
                    dbtypeName: 'PLACE',
                    isPrimaryKey: false,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    isMultivalueField: false,
                },
                {
                    configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                    name: 'PhysicalColumn',
                    configObjectType: 'PhysicalColumn',
                    createdBy: '1111',
                    isDeleted: 0,
                    itemDescription: 'PhysicalColumn',
                    creationDate: null,
                    projectId: 0,
                    updatedBy: '1111',
                    updationDate: null,
                    deletionDate: null,
                    dataType: 'TIMESTAMP',
                    length: 40,
                    dbCode: 'CURR_DATE',
                    dbtypeName: 'CURR_DATE',
                    isPrimaryKey: false,
                    isVirtual: false,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    isMultivalueField: false,
                },
                {
                    configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                    name: 'PhysicalColumn',
                    configObjectType: 'PhysicalColumn',
                    createdBy: '1111',
                    isDeleted: 0,
                    itemDescription: 'PhysicalColumn',
                    creationDate: null,
                    projectId: 0,
                    updatedBy: '1111',
                    updationDate: null,
                    deletionDate: null,
                    dataType: 'string',
                    length: 40,
                    dbCode: 'COUNTRY',
                    dbtypeName: 'COUNTRY',
                    isPrimaryKey: false,
                    isVirtual: true,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    isMultivalueField: false,
                },
                {
                    configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                    name: 'PhysicalColumn',
                    configObjectType: 'PhysicalColumn',
                    createdBy: '1111',
                    isDeleted: 0,
                    itemDescription: 'PhysicalColumn',
                    creationDate: null,
                    projectId: 0,
                    updatedBy: '1111',
                    updationDate: null,
                    deletionDate: null,
                    dataType: 'number',
                    length: 40,
                    dbCode: 'EXPENSE',
                    dbtypeName: 'EXPENSE',
                    isPrimaryKey: false,
                    isVirtual: true,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    isMultivalueField: false,
                },
                {
                    configObjectId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
                    name: 'PhysicalColumn',
                    configObjectType: 'PhysicalColumn',
                    createdBy: '1111',
                    isDeleted: 0,
                    itemDescription: 'PhysicalColumn',
                    creationDate: null,
                    projectId: 0,
                    updatedBy: '1111',
                    updationDate: null,
                    deletionDate: null,
                    dataType: 'NUMBER',
                    length: 40,
                    dbCode: 'Action',
                    dbtypeName: 'Action',
                    isPrimaryKey: false,
                    isVirtual: true,
                    isKey: false,
                    isPhysicalColumnMandatory: false,
                    isVerticalField: false,
                    isUnique: false,
                    isMandatory: false,
                    isMultivalueField: false,
                },
            ],
        },
    ],
    nodeBusinessRules: [{
            configObjectId: '6ef43bb3-e435-44fd-a762-d8ac6a9532',
            executionType: 'Javascript',
            rule: 'input["PLACE"] =input["NAME"] + " " + input["PLACE"]',
            order: 5,
            name: 'Concatinating Name with Place',
        },
        {
            configObjectId: '6ef43bb3-e435-44fd-a762-d8ac532',
            executionType: 'Javascript',
            rule: 'let childRecords = input[\'AppEngChildEntity:Employee\'];'
                + 'let companyExpense = 0;'
                + 'for(let record of childRecords){'
                + 'companyExpense=companyExpense+record["INCOME"];}'
                + 'input["EXPENSE"]=companyExpense\n;'
                + 'let companyExpenseRecords = [];\n'
                + 'const expense = {"COMPANY_ID":input["ID"],"EXPENSE":companyExpense};\n'
                + 'companyExpenseRecords.push(expense);\n'
                + 'input[\'AppEngChildEntity:CompanyExpense\']=companyExpenseRecords',
            order: 10,
            name: 'Expense of Company will be Sum of Employee Income',
        }],
};
//# sourceMappingURL=base.node.js.map