"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    FormField: {
        columnDataPreprocessors: [
            {
                configObjectId: 'd4588e5b-153c-4dcb-a9d1-f41819587ca2',
                name: 'Calculate Total Families Without Toilet',
                configObjectType: 'ColumnDataPreprocessor',
                preProcessorBean: null,
                isMultiValue: false,
                excecutionType: 'Javascript',
                jsCode: 'var fam = (input[0]["TOTAL_FAMILIES"] ?parseInt(input[0]["TOTAL_FAMILIES"]):0)\n-(input[0]["FAMILIES_HAVING_TOILETS"] ?parseInt(input[0]["FAMILIES_HAVING_TOILETS"]):0);\n\ninput[0]["FAMILIES_WITHOUT_TOILETS"] = parseInt(fam);',
                createdBy: '1114',
                isDeleted: 0,
                itemDescription: null,
                creationDate: '2017-06-15T18:30:00.000Z',
                projectId: 0,
                updatedBy: null,
                updationDate: null,
                deletionDate: null,
                order: null,
            },
        ],
    },
};
//# sourceMappingURL=formfield.data.preprocessor.js.map