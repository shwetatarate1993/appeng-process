declare const _default: {
    dataSetRelColumns: {
        childColumn: {
            dbCode: string;
            __typename: string;
        };
        parentColumn: {
            dbCode: string;
            __typename: string;
        };
    }[];
};
export default _default;
//# sourceMappingURL=dataset.rel.d.ts.map