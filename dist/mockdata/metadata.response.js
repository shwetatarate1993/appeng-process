"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const child_nodes_multivalued_1 = __importDefault(require("./child.nodes.multivalued"));
const dataset_rel_1 = __importDefault(require("./dataset.rel"));
exports.default = {
    data: {
        DataSetRelEntityColumn: dataset_rel_1.default,
        ChildCompositeEntityNodes: child_nodes_multivalued_1.default,
        CompositeEntityNode: { sankalp: 'Dhekwar' },
    },
};
//# sourceMappingURL=metadata.response.js.map