"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_db_1 = require("appeng-db");
const fetch_mock_1 = __importDefault(require("fetch-mock"));
const mockdata_1 = require("../mockdata");
const services_1 = require("../services");
class AppengProcessConfig {
    constructor() {
        /** No Op */
    }
    static get INSTANCE() {
        return AppengProcessConfig.inboundProcess;
    }
    static get DATA_PREPROCESSOR_INSTANCE() {
        return AppengProcessConfig.dataPreprocessor;
    }
    static configure(config, links, configuredDialect) {
        if (!AppengProcessConfig.inboundProcess
            && !AppengProcessConfig.dataPreprocessor) {
            AppengProcessConfig.appengMetaURI = links.appeng_meta;
            appeng_db_1.AppengDBConfig.configure(config, configuredDialect);
        }
        if (!AppengProcessConfig.inboundProcess) {
            const inboundProcess = services_1.createInboundProcess(appeng_db_1.AppengDBConfig.INSTANCE);
            AppengProcessConfig.inboundProcess = inboundProcess;
        }
        if (!AppengProcessConfig.dataPreprocessor) {
            const dataPreprocessor = services_1.createDataPreProcessor(appeng_db_1.AppengDBConfig.INSTANCE);
            AppengProcessConfig.dataPreprocessor = dataPreprocessor;
        }
    }
}
exports.default = AppengProcessConfig;
const mockGraphqlCall = (uri) => {
    fetch_mock_1.default.restore();
    if (process.env.NODE_ENV === 'test') {
        fetch_mock_1.default.mock(uri, mockdata_1.metadataResponse).spy();
    }
};
//# sourceMappingURL=appeng.process.config.js.map