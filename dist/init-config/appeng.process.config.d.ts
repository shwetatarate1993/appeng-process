import { DataPreProcessor, InboundProcess } from '../services';
export default class AppengProcessConfig {
    static readonly INSTANCE: InboundProcess;
    static readonly DATA_PREPROCESSOR_INSTANCE: DataPreProcessor;
    static appengMetaURI: string;
    static configure(config: any, links: any, configuredDialect: string): void;
    private static inboundProcess;
    private static dataPreprocessor;
    private constructor();
}
//# sourceMappingURL=appeng.process.config.d.ts.map