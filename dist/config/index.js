"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const convict_1 = __importDefault(require("convict"));
const development_1 = __importDefault(require("./env/development"));
const production_1 = __importDefault(require("./env/production"));
const serverurl = 'http://localhost:8892';
// Define a schema
const config = convict_1.default({
    env: {
        doc: 'The application environment.',
        format: ['production', 'development', 'test'],
        default: 'development',
        env: 'NODE_ENV',
    },
    cache: {
        doc: 'Cache Implementation',
        format: String,
        default: 'nodecache',
    },
    secret: {
        doc: 'Secret key',
        format: '*',
        default: 'defaultsecret',
        env: 'SECRET_KEY',
    },
    ip: {
        doc: 'The IP address to bind.',
        format: 'ipaddress',
        default: '127.0.0.1',
        env: 'IP_ADDRESS',
    },
    port: {
        doc: 'The port to bind.',
        format: 'port',
        default: 8892,
        env: 'PORT',
        arg: 'port',
    },
    db: {
        mock: {
            dialect: 'sqlite3',
            connection: {
                doc: 'SQL Lite connection type',
                format: '*',
                default: ':memory:',
            },
        },
        METADB: {
            dialect: 'mysql',
            host: {
                doc: 'Database host name/IP',
                format: '*',
                default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
            },
            name: {
                doc: 'Database name',
                format: String,
                default: 'dummy',
            },
            username: {
                doc: 'Database Username',
                format: String,
                default: 'awsdevmaster',
            },
            password: {
                doc: 'Database Password',
                format: String,
                default: 'awsinfo2018',
            },
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000,
            },
        },
    },
    links: {},
});
// Load environment dependent configuration
const envName = config.get('env');
switch (envName) {
    case 'production':
        config.load(production_1.default);
        break;
    case 'development':
        config.load(development_1.default);
        break;
    default:
    // do nothing
}
// Perform validation
config.validate({ allowed: 'strict' });
exports.default = config;
//# sourceMappingURL=index.js.map