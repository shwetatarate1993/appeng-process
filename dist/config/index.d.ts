import { default as convict } from 'convict';
declare const config: convict.Config<{
    env: string;
    cache: string;
    secret: string;
    ip: string;
    port: number;
    db: {
        mock: any;
        METADB: any;
    };
    links: unknown;
}>;
export default config;
//# sourceMappingURL=index.d.ts.map