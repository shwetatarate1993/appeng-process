import { ApolloClient } from 'apollo-client';
export declare const manipulateQuery: (query1: any) => any;
export declare const createApolloClient: (uri: string) => ApolloClient<unknown>;
//# sourceMappingURL=metadata.utils.d.ts.map