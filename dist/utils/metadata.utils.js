"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_client_1 = require("apollo-client");
const apollo_link_http_1 = require("apollo-link-http");
/* tslint:disable-next-line:no-var-requires */
const fetch = require('node-fetch');
const apollo_cache_inmemory_1 = require("apollo-cache-inmemory");
exports.manipulateQuery = (query1) => {
    let query = query1.loc.source.body;
    query = query.trim();
    query = query.substring(query.indexOf("{"));
    query = query.substring(0, query.length);
    return query;
};
exports.createApolloClient = (uri) => {
    const client = new apollo_client_1.ApolloClient({
        ssrMode: true,
        link: new apollo_link_http_1.HttpLink({
            uri,
            fetch,
        }),
        cache: new apollo_cache_inmemory_1.InMemoryCache(),
    });
    return client;
};
//# sourceMappingURL=metadata.utils.js.map