import { ServiceOrchestrator } from 'appeng-db';
import { PhysicalData } from '../models';
export declare const processPhysicalData: (physicalDatas: PhysicalData[], serviceOrchestrator: ServiceOrchestrator, primaryEntityId: string) => Promise<{
    pk: any;
    mode: any;
}>;
//# sourceMappingURL=process.physical.data.d.ts.map