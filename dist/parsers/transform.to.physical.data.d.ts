import { ServiceOrchestrator } from 'appeng-db';
import { EntityGroupData, PhysicalData } from '../models';
export declare const transformToPhysicalData: (entityGroupData: EntityGroupData, serviceOrchestrator: ServiceOrchestrator) => Promise<PhysicalData[]>;
//# sourceMappingURL=transform.to.physical.data.d.ts.map