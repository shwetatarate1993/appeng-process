"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const mockdata_1 = require("../../mockdata");
const transform_to_logical_data_1 = require("../transform.to.logical.data");
init_config_1.AppengProcessConfig.configure(config_1.default.get(constant_1.DB), config_1.default.get(constant_1.LINKS), 'sqlite3');
test('transformToLogicalData parser', async () => {
    const input = JSON.parse(JSON.stringify(mockdata_1.apiRequestMock));
    const serviceOrchestrator = init_config_1.AppengProcessConfig.INSTANCE.serviceOrchestrator;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    const singleSelect = servicesMock.expects('selectSingleRecord').atLeast(1).returns({});
    const output = await transform_to_logical_data_1.transformToLogicalData(input, {});
    expect(output).toHaveProperty('logicalData');
    const parentAppData = output.logicalData.appData.data[0];
    const parentApiData = mockdata_1.apiRequestMock.baseEntity.records[0];
    expect(parentAppData.NAME === parentApiData.NAME).toBe(true);
    expect(parentAppData.COMPANY_TITLE === parentApiData.TITLE).toBe(true);
    expect(parentAppData.CURR_DATE === parentApiData.CURR_DATE)
        .toBe(false);
    expect(output.logicalData.childLogicalData.length === 1).toBe(true);
    expect(output.logicalData.childLogicalData[0].childLogicalData.length
        === 1).toBe(true);
    sinon_1.default.restore();
    servicesMock.restore();
});
//# sourceMappingURL=transform.to.logical.data.test.js.map