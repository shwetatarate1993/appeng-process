"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const mockdata_1 = require("../../mockdata");
const process_physical_data_1 = require("../process.physical.data");
init_config_1.AppengProcessConfig.configure(config_1.default.get(constant_1.DB), config_1.default.get(constant_1.LINKS), 'sqlite3');
test('process physical datas', async () => {
    const mockedPhysicalData = JSON.parse(JSON.stringify(mockdata_1.physicalDataMock));
    const serviceOrchestrator = init_config_1.AppengProcessConfig.INSTANCE.serviceOrchestrator;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    servicesMock.expects('insert').exactly(1).returns(1);
    servicesMock.expects('update').once().returns(1);
    servicesMock.expects('delete').never();
    const output = await process_physical_data_1.processPhysicalData(mockedPhysicalData, serviceOrchestrator, mockedPhysicalData[0].physicalEntity.configObjectId);
    console.log('output...', output);
    servicesMock.verify();
    sinon_1.default.assert.match(output.pk, 2);
    servicesMock.restore();
});
//# sourceMappingURL=process.physical.data.test.js.map