"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mockdata_1 = require("../../mockdata");
const process_logical_data_1 = require("../process.logical.data");
test('process logical data', async () => {
    const entityGroupData = JSON.parse(JSON.stringify(mockdata_1.entityGroupDataMock));
    const output = await process_logical_data_1.processLogicalData(entityGroupData, null);
    const parentData = output.logicalData.appData.data[0];
    const childData = output.logicalData.childLogicalData[0].appData.data[0];
    expect(parentData.NAME === childData.COMPANY_NAME).toBe(true);
    expect(parentData.count === 10).toBe(true);
});
//# sourceMappingURL=process.logical.data.test.js.map