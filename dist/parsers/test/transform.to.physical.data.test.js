"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const mockdata_1 = require("../../mockdata");
const transform_to_physical_data_1 = require("../transform.to.physical.data");
init_config_1.AppengProcessConfig.configure(config_1.default.get(constant_1.DB), config_1.default.get(constant_1.LINKS), 'sqlite3');
test('transform To Physical Data(muiltilevel)', async () => {
    const mockesData = JSON.parse(JSON.stringify(mockdata_1.entityGroupDataMock));
    const serviceOrchestrator = init_config_1.AppengProcessConfig.INSTANCE.serviceOrchestrator;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    const singleSelect = servicesMock.expects('selectSingleRecord');
    singleSelect.onFirstCall().returns({ NAME: '2018', ID: 2 });
    singleSelect.atLeast(1).returns({});
    servicesMock.expects('selectRecords').never();
    servicesMock.expects('createSequence').atLeast(1).returns({ ID: 1 });
    const output = await transform_to_physical_data_1.transformToPhysicalData(mockesData, serviceOrchestrator);
    servicesMock.verify();
    sinon_1.default.assert.match(output[0].dbOperationType, constant_1.OperationType.UPDATE);
    sinon_1.default.assert.match(output[1].dbOperationType, constant_1.OperationType.INSERT);
    sinon_1.default.assert.match(output[0].newData.ID, output[1].newData.COMPANY_ID);
    sinon_1.default.assert.match(output[1].newData.ID, output[2].newData.EMPLOYEE_ID);
    sinon_1.default.assert.match(output.length, 3);
    servicesMock.restore();
});
test('multivalued mapping entity', async () => {
    const mockesData = JSON.parse(JSON.stringify(mockdata_1.multiValuedPEMock));
    const serviceOrchestrator = init_config_1.AppengProcessConfig.INSTANCE.serviceOrchestrator;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    servicesMock.expects('selectSingleRecord').never();
    servicesMock.expects('selectRecords').atLeast(1).returns([{ ID: 1, NAME: '2021' }]);
    servicesMock.expects('createSequence').atLeast(1).returns({ ID: 1 });
    const output = await transform_to_physical_data_1.transformToPhysicalData(mockesData, serviceOrchestrator);
    servicesMock.verify();
    sinon_1.default.assert.match(output[0].dbOperationType, constant_1.OperationType.DELETE);
    sinon_1.default.assert.match(output[1].dbOperationType, constant_1.OperationType.INSERT);
    sinon_1.default.assert.match(output.length, 4);
    servicesMock.restore();
});
//# sourceMappingURL=transform.to.physical.data.test.js.map