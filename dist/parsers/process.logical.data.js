"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_flow_1 = require("appeng-flow");
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
const config_data_structure_1 = require("../config.data.structure");
const constant_1 = require("../constant");
const init_config_1 = require("../init-config");
const mockdata_1 = require("../mockdata");
const models_1 = require("../models");
const info_commons_1 = require("info-commons");
const appeng_meta_1 = __importDefault(require("appeng-meta"));
const metadata_utils_1 = require("../utils/metadata.utils");
let client;
exports.processLogicalData = async (entityGroupData, serviceOrchestrator) => {
    console.log('processLogicalData...');
    if (info_commons_1.APP_ENV === 'server') {
        client = metadata_utils_1.createApolloClient(init_config_1.AppengProcessConfig.appengMetaURI);
    }
    const childLogicalDatas = await getLogicalDataWithDerivedValues(entityGroupData.logicalData);
    const logicalData = new models_1.LogicalData(entityGroupData.logicalData.ceNode, entityGroupData.logicalData.appData, childLogicalDatas);
    const newLogicalData = await processBusinessRules(logicalData, entityGroupData.referenceData, serviceOrchestrator);
    const groupDataWithDerviedValues = new models_1.EntityGroupData(newLogicalData, entityGroupData.referenceData, entityGroupData.action);
    return groupDataWithDerviedValues;
};
const processBusinessRules = async (logicalData, referenceData, serviceOrchestrator) => {
    if (hasRules(logicalData)) {
        const processedData = getInputDataForBusinessRule(logicalData, referenceData);
        await executeRules(logicalData, processedData, serviceOrchestrator);
        logicalData = await populateRuleOuputData(processedData, logicalData);
    }
    return logicalData;
};
const getLogicalDataWithDerivedValues = async (logicalData) => {
    const logicalDatas = [];
    for (const child of logicalData.childLogicalData) {
        const childEntity = child.ceNode.entity;
        const derivedColumns = childEntity.logicalColumns.filter((col) => col.isDerived);
        const dataList = cloneDeep_1.default(child.appData.data);
        if (derivedColumns.length > 0) {
            derivedColumns.map((col) => {
                const sourceCol = col.sourceColumn;
                const parentData = logicalData.appData.data[0];
                const derivedVal = parentData[sourceCol.dbCode];
                dataList.map((data) => {
                    data[col.dbCode] = derivedVal;
                });
            });
        }
        let childLogicalData = [];
        const appData = new models_1.AppData(dataList);
        if (child.childLogicalData.length > 0) {
            childLogicalData = await getLogicalDataWithDerivedValues(child);
        }
        logicalDatas.push(new models_1.LogicalData(child.ceNode, appData, childLogicalData));
    }
    return logicalDatas;
};
const hasRules = (logicalData) => {
    if (logicalData.ceNode.nodeBusinessRules && logicalData.ceNode.nodeBusinessRules.length) {
        return true;
    }
    return logicalData.childLogicalData.some((childLD) => {
        if (childLD.ceNode.nodeBusinessRules && childLD.ceNode.nodeBusinessRules.length) {
            return true;
        }
        return false;
    });
};
const executeRules = async (logicalData, processedData, serviceOrchestrator) => {
    if (logicalData.ceNode.nodeBusinessRules && logicalData.ceNode.nodeBusinessRules.length) {
        processedData =
            await appeng_flow_1.businessRulesExecutor(logicalData.ceNode.nodeBusinessRules, processedData, serviceOrchestrator);
    }
    for (const childLD of logicalData.childLogicalData) {
        if (childLD.ceNode.nodeBusinessRules && childLD.ceNode.nodeBusinessRules.length) {
            processedData =
                await appeng_flow_1.businessRulesExecutor(childLD.ceNode.nodeBusinessRules, processedData, serviceOrchestrator);
        }
    }
};
const getInputDataForBusinessRule = (logicalData, referenceData) => {
    const formattedData = cloneDeep_1.default(logicalData.appData.data[0]);
    Object.assign(formattedData, referenceData);
    const childLogicalDatas = logicalData.childLogicalData;
    for (const childLD of childLogicalDatas) {
        const childData = cloneDeep_1.default(childLD.appData.data);
        childData.map((data) => Object.assign(data, referenceData));
        const childKey = constant_1.CHILD_ENTITY_PREFIX + childLD.ceNode.entity.dbTypeName;
        formattedData[childKey] = childData;
    }
    return formattedData;
};
const populateRuleOuputData = async (ruleOutputData, logicalData) => {
    const newChildLogicalDatas = [];
    processExistingChildLogicalData(newChildLogicalDatas, logicalData.childLogicalData, ruleOutputData);
    let hiddenNodes = null;
    //if (process.env.NODE_ENV === 'test') {
       // hiddenNodes = JSON.parse(JSON.stringify(mockdata_1.hiddenNodesMock));
    //}
   // else {
        hiddenNodes = await getHiddenNodes(logicalData.ceNode.configObjectId);
    //}
    if (hiddenNodes && hiddenNodes.length) {
        generateLogicalDataForHiddenNodes(newChildLogicalDatas, hiddenNodes, ruleOutputData);
    }
    const parentData = [];
    parentData.push(ruleOutputData);
    const appData = new models_1.AppData(parentData);
    return new models_1.LogicalData(logicalData.ceNode, appData, newChildLogicalDatas);
};
const processExistingChildLogicalData = (newChildLogicalDatas, existingChildLogicalData, ruleOutputData) => {
    existingChildLogicalData.map((childLD) => {
        generateNewLogicalData(childLD.ceNode, newChildLogicalDatas, ruleOutputData);
    });
};
const generateLogicalDataForHiddenNodes = (newChildLogicalDatas, hiddenNodes, ruleOutputData) => {
    hiddenNodes.map((node) => {
        generateNewLogicalData(node, newChildLogicalDatas, ruleOutputData);
    });
};
const generateNewLogicalData = (node, newChildLogicalDatas, ruleOutputData) => {
    const childKey = constant_1.CHILD_ENTITY_PREFIX + node.entity.dbTypeName;
    if (ruleOutputData[childKey]) {
        const childAppData = new models_1.AppData([...ruleOutputData[childKey]]);
        newChildLogicalDatas.push(new models_1.LogicalData(node, childAppData, []));
        delete ruleOutputData[childKey];
    }
};
const getHiddenNodes = async (nodeId) => {
    try {
        if (info_commons_1.APP_ENV === 'desktop') {
            let query = metadata_utils_1.manipulateQuery(config_data_structure_1.GET_HIDDEN_NODES);
            query = query.replace("$id", '"' + nodeId + '"');
            const { data } = await appeng_meta_1.default(query);
            return data.HiddenNodes;
        }
        else {
            const { data } = await client.query({
                query: config_data_structure_1.GET_HIDDEN_NODES,
                variables: { id: nodeId },
            });
            return data.HiddenNodes;
        }
    }
    catch (error) {
        throw error;
    }
};
//# sourceMappingURL=process.logical.data.js.map