"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
const uuid_1 = require("uuid");
const config_data_structure_1 = require("../config.data.structure");
const constant_1 = require("../constant");
const init_config_1 = require("../init-config");
const mockdata_1 = require("../mockdata");
const models_1 = require("../models");
const appengutil_1 = require("../utils/appengutil");
const info_commons_1 = require("info-commons");
const appeng_meta_1 = __importDefault(require("appeng-meta"));
const metadata_utils_1 = require("../utils/metadata.utils");
let client;
let dbServices;
exports.transformToPhysicalData = async (entityGroupData, serviceOrchestrator) => {
    console.log('transformToPhysicalData...');
    if (info_commons_1.APP_ENV === 'server') {
        client = metadata_utils_1.createApolloClient(init_config_1.AppengProcessConfig.appengMetaURI);
    }
    dbServices = serviceOrchestrator;
    const allPhysicalDatas = await extractPhysicalData(entityGroupData);
    return allPhysicalDatas;
};
const extractPhysicalData = async (entityGroupData) => {
    const physicalEntities = entityGroupData.logicalData.ceNode.physicalDataEntities;
    const parentRecords = cloneDeep_1.default(entityGroupData.logicalData.appData.data);
    const allPhysicalDatas = [];
    const parentRecord = await getInterWeavedRecordsInternal(physicalEntities, parentRecords, allPhysicalDatas, {}, entityGroupData.referenceData);
    const childLogicalData = entityGroupData.logicalData.childLogicalData;
    if (childLogicalData.length > 0) {
        const keyList = getAllKeysDBCode(physicalEntities);
        await getChildEntityPhysicalData(entityGroupData.logicalData.ceNode, childLogicalData, parentRecord, allPhysicalDatas, keyList, entityGroupData.referenceData);
    }
    return allPhysicalDatas;
};
const getChildEntityPhysicalData = async (parentNode, logicalDatas, parentRecord, allPhysicalDatas, allKeys, referenceData) => {
    const keyData = {};
    for (const key of allKeys) {
        if (parentRecord[key] !== undefined) {
            keyData[key] = parentRecord[key];
        }
    }
    for (const logicalData of logicalDatas) {
        const node = logicalData.ceNode;
        const foreignKeys = await getForeignKeyData(parentNode, node, parentRecord);
        Object.assign(foreignKeys, keyData);
        const masterRecord = await getInterWeavedRecordsInternal(node.physicalDataEntities, cloneDeep_1.default(logicalData.appData.data), allPhysicalDatas, foreignKeys, referenceData);
        const childLogicalDatas = logicalData.childLogicalData;
        if (childLogicalDatas.length > 0) {
            const keyList = getAllKeysDBCode(node.physicalDataEntities);
            await getChildEntityPhysicalData(node, childLogicalDatas, masterRecord, allPhysicalDatas, keyList, referenceData);
        }
    }
};
const getForeignKeyData = async (parentNode, childNode, parentRecord) => {
    const foreignKeys = {};
    const parentEntityId = parentNode.entity.configObjectId;
    const childEntityId = childNode.entity.configObjectId;
    let dataSetRels = null;
    //if (process.env.NODE_ENV === 'test') {
      //  dataSetRels = JSON.parse(JSON.stringify(mockdata_1.datasetRelMock));
    //}
   // else {
        dataSetRels = await getDatasetRel(parentEntityId, childEntityId);
   // }
    if (dataSetRels && dataSetRels.dataSetRelColumns) {
        for (const rel of dataSetRels.dataSetRelColumns) {
            if (parentRecord[rel.parentColumn.dbCode] !== undefined) {
                foreignKeys[rel.childColumn.dbCode] =
                    parentRecord[rel.parentColumn.dbCode];
            }
        }
    }
    return foreignKeys;
};
const getAllKeysDBCode = (physicalEntities) => {
    const keyDBCodeList = [];
    physicalEntities.map((pe) => {
        pe.physicalColumns.filter((pc) => pc.isKey)
            .forEach((pc) => keyDBCodeList.push(pc.dbCode));
    });
    return keyDBCodeList;
};
const getInterWeavedRecordsInternal = async (physicalEntities, records, allPhysicalDatas, parentData, referenceData) => {
    const parentRecord = {};
    for (const record of records) {
        const uniqueKeysMap = {};
        for (const pe of physicalEntities) {
            if (pe.expressionAvailable &&
                !appengutil_1.isExpressionResolved(pe.accessibilityRegex, record)) {
                continue;
            }
            const physicalEntity = Object.assign(new models_1.PhysicalEntity(), pe);
            const { dbCodeList, physicalColumnMandatoryList } = physicalEntity.getPropertyMap();
            if (!isMandatoryDataAvailable(physicalColumnMandatoryList, record)) {
                continue;
            }
            let isNew = true;
            const requiredData = getFilteredData(record, dbCodeList);
            const parentDataKeys = Object.keys(parentData);
            for (const key of parentDataKeys) {
                if (!requiredData[key])
                    requiredData[key] = parentData[key];
            }
            Object.assign(requiredData, uniqueKeysMap, referenceData);
            if (pe.isMultivalueMapping) {
                const mappingPhysicalData = await getMappingPhysicalDatas(pe, requiredData);
                allPhysicalDatas.push.apply(allPhysicalDatas, mappingPhysicalData);
                continue;
            }
            else {
                let oldRecord = {};
                try {
                    oldRecord = await dbServices.selectSingleRecord(pe, requiredData);
                }
                catch (error) {
                    throw error;
                }
                if (Object.keys(oldRecord).length > 0) {
                    isNew = false;
                    const auditData = getAuditData(constant_1.OperationType.UPDATE);
                    const mergedNewValues = Object.assign(Object.assign({}, oldRecord), requiredData, auditData);
                    Object.assign(parentRecord, mergedNewValues);
                    allPhysicalDatas.push(new models_1.PhysicalData(constant_1.OperationType.UPDATE, pe, mergedNewValues));
                }
                if (isNew) {
                    const uniqueKeys = await generateUniqueKey(pe, requiredData);
                    Object.entries(uniqueKeys).forEach(([key, value]) => {
                        if (!requiredData[key]) {
                            requiredData[key] = value;
                            uniqueKeysMap[key] = value;
                        }
                    });
                    const auditData = getAuditData(constant_1.OperationType.INSERT);
                    const mergedData = Object.assign(requiredData, auditData);
                    Object.assign(parentRecord, requiredData);
                    allPhysicalDatas.push(new models_1.PhysicalData(constant_1.OperationType.INSERT, pe, mergedData));
                }
            }
        }
    }
    return parentRecord;
};
const getAuditData = (operationType) => {
    const auditData = {};
    switch (operationType) {
        case constant_1.OperationType.INSERT:
            auditData.CREATIONDATE = new Date();
            auditData.ISDELETED = 0;
            auditData.CHANGE_TIMESTAMP = new Date().getTime();
            break;
        case constant_1.OperationType.UPDATE:
            auditData.UPDATIONDATE = new Date();
            auditData.ISDELETED = 0;
            auditData.CHANGE_TIMESTAMP = new Date().getTime();
            break;
        case constant_1.OperationType.DELETE:
            auditData.DELETIONDATE = new Date();
            auditData.ISDELETED = 1;
            auditData.CHANGE_TIMESTAMP = new Date().getTime();
            break;
    }
    return auditData;
};
const getMappingPhysicalDatas = async (physicalEntity, data) => {
    const mappingPhysicalData = [];
    const multivaluedField = physicalEntity.physicalColumns.find((pc) => pc.isMultivalueField);
    if (multivaluedField && multivaluedField.dbCode) {
        const multiValuedDBCode = multivaluedField.dbCode;
        const selectedOptions = data[multiValuedDBCode] ? data[multiValuedDBCode]
            .split(constant_1.Separator.ListSeparator) : [];
        delete data[multiValuedDBCode];
        const needToDeleteRecords = [];
        // fetching all existing options
        let existingOptions = [];
        try {
            existingOptions = await dbServices.selectRecords(physicalEntity, data);
        }
        catch (error) {
            throw error;
        }
        // seperating options to insert and to delete
        for (const existingValue of existingOptions) {
            if (selectedOptions.includes(existingValue[multiValuedDBCode])) {
                const index = selectedOptions.indexOf(existingValue[multiValuedDBCode]);
                selectedOptions.splice(index, 1);
            }
            else {
                needToDeleteRecords.push(Object.assign({}, existingValue));
            }
        }
        // creating physical data for options which needed to delete
        let physicalData = getPhysicalDataToDelete(needToDeleteRecords, physicalEntity);
        mappingPhysicalData.push.apply(mappingPhysicalData, physicalData);
        // creating physical data for options which needed to insert
        physicalData = await getPhysicalDataToInsert(selectedOptions, data, multiValuedDBCode, physicalEntity);
        mappingPhysicalData.push.apply(mappingPhysicalData, physicalData);
    }
    return mappingPhysicalData;
};
const getPhysicalDataToInsert = async (selectedOptions, data, multiValuedDBCode, physicalEntity) => {
    const physicalData = [];
    for (const option of selectedOptions) {
        const newData = cloneDeep_1.default(data);
        newData[multiValuedDBCode] = option;
        const uniqueKeys = await generateUniqueKey(physicalEntity, newData);
        const auditData = getAuditData(constant_1.OperationType.INSERT);
        const mergedData = Object.assign(newData, uniqueKeys, auditData);
        physicalData.push(new models_1.PhysicalData(constant_1.OperationType.INSERT, physicalEntity, mergedData));
    }
    return physicalData;
};
const getPhysicalDataToDelete = (needToDeleteRecords, physicalEntity) => {
    const physicalData = [];
    for (const record of needToDeleteRecords) {
        const auditData = getAuditData(constant_1.OperationType.DELETE);
        Object.assign(record, auditData);
        physicalData.push(new models_1.PhysicalData(constant_1.OperationType.DELETE, physicalEntity, record));
    }
    return physicalData;
};
const generateUniqueKey = async (physicalEntity, data) => {
    const uniqueKeys = {};
    const queryList = physicalEntity.sequenceQID
        .split(constant_1.Separator.Separator);
    for (const query of queryList) {
        switch (uniqueKeyLogic(query)) {
            case constant_1.UniqueKey.UUID:
                uniqueKeys[query.split(constant_1.Separator.Colon)[1]] = uuid_1.v4();
                break;
            case constant_1.UniqueKey.SQL:
                let sequence = {};
                try {
                    sequence = await dbServices.createSequence(physicalEntity, data);
                }
                catch (error) {
                    throw error;
                }
                Object.assign(uniqueKeys, sequence);
                break;
        }
    }
    return uniqueKeys;
};
const uniqueKeyLogic = (query) => {
    if (query !== '') {
        const tokens = query.split(constant_1.Separator.Colon);
        if (tokens.length === 1) {
            return constant_1.UniqueKey.SQL;
        }
        else {
            return tokens[0];
        }
    }
    return '';
};
const isMandatoryDataAvailable = (physicalColumnMandatoryList, record) => {
    let isRequiredDataAvailable = true;
    physicalColumnMandatoryList.some((column) => {
        if (!record[column]) {
            isRequiredDataAvailable = false;
            return true;
        }
        return false;
    });
    return isRequiredDataAvailable;
};
const getFilteredData = (record, dbCodeList) => {
    const data = {};
    Object.entries(record).forEach(([key, value]) => {
        if (dbCodeList.includes(key)) {
            data[key] = value;
        }
    });
    return data;
};
const getDatasetRel = async (parentEntityId, childEntityId) => {
    try {
        if (info_commons_1.APP_ENV === 'desktop') {
            let query = metadata_utils_1.manipulateQuery(config_data_structure_1.GET_DATASET_REL);
            query = query.replace("$parentEntityId", '"' + parentEntityId + '"')
                .replace("$childEntityId", '"' + childEntityId + '"');
            const { data } = await appeng_meta_1.default(query);
            return data.DataSetRelEntityColumn;
        }
        else {
            const { data } = await client.query({
                query: config_data_structure_1.GET_DATASET_REL,
                variables: {
                    parentEntityId,
                    childEntityId,
                },
            });
            return data.DataSetRelEntityColumn;
        }
    }
    catch (error) {
        throw error;
    }
};
//# sourceMappingURL=transform.to.physical.data.js.map