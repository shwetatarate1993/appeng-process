import { APIRequest, EntityGroupData } from '../models';
export declare const transformToLogicalData: (apiRequest: APIRequest, referenceData: any) => Promise<EntityGroupData>;
//# sourceMappingURL=transform.to.logical.data.d.ts.map