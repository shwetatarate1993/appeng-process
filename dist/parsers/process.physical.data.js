"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constant_1 = require("../constant");
let dbServices;
exports.processPhysicalData = async (physicalDatas, serviceOrchestrator, primaryEntityId) => {
    console.log('processPhysicalData...');
    dbServices = serviceOrchestrator;
    for (const physicalData of physicalDatas) {
        switch (physicalData.dbOperationType) {
            case constant_1.OperationType.INSERT:
                try {
                    const primaryKey = await dbServices.insert(physicalData.physicalEntity, physicalData.newData);
                }
                catch (error) {
                    throw error;
                }
                break;
            case constant_1.OperationType.UPDATE:
                try {
                    const affectedRows = await dbServices.update(physicalData.physicalEntity, physicalData.newData);
                }
                catch (error) {
                    throw error;
                }
                break;
            case constant_1.OperationType.DELETE:
                try {
                    const affectedRowsDelete = await dbServices.delete(physicalData.physicalEntity, physicalData.newData);
                }
                catch (error) {
                    throw error;
                }
                break;
        }
    }
    let masterPhysicalData = null;
    let parentPrimaryKey = null;
    let mode = null;
    let pk = null;
    if (physicalDatas.length > 0) {
        masterPhysicalData = physicalDatas.find((pd) => pd.physicalEntity.configObjectId === primaryEntityId);
        parentPrimaryKey =
            masterPhysicalData.physicalEntity.physicalColumns.find((pc) => pc.isPrimaryKey).dbCode;
        mode = masterPhysicalData.dbOperationType;
        pk = masterPhysicalData.newData[parentPrimaryKey];
    }
    return { pk, mode };
};
//# sourceMappingURL=process.physical.data.js.map