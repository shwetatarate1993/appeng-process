"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_data_structure_1 = require("../config.data.structure");
const constant_1 = require("../constant");
const init_config_1 = require("../init-config");
const mockdata_1 = require("../mockdata");
const models_1 = require("../models");
const info_commons_1 = require("info-commons");
const appeng_meta_1 = __importDefault(require("appeng-meta"));
const metadata_utils_1 = require("../utils/metadata.utils");
let client;
exports.transformToLogicalData = async (apiRequest, referenceData) => {
    console.log('transformToLogicalData...');
    if (info_commons_1.APP_ENV === 'server') {
        client = metadata_utils_1.createApolloClient(init_config_1.AppengProcessConfig.appengMetaURI);
    }
    const serviceOrchestrator = init_config_1.AppengProcessConfig.INSTANCE.serviceOrchestrator;
    let baseNode = null;
    //if (process.env.NODE_ENV === 'test') {
       // baseNode = JSON.parse(JSON.stringify(mockdata_1.baseNodeMock));
   // }
    //else {
        baseNode = await getParentNode(apiRequest.baseEntity.nodeId);
   // }
    const dataList = await processRecords(baseNode, apiRequest.baseEntity.records, serviceOrchestrator);
    const appData = new models_1.AppData(dataList);
    const logicalDatas = await getChildEntitiesLogicalData(apiRequest.baseEntity.childEntities, apiRequest.baseEntity.nodeId, serviceOrchestrator);
    const logicalData = new models_1.LogicalData(baseNode, appData, logicalDatas);
    referenceData.compositeEntityAction = apiRequest.action;
    return new models_1.EntityGroupData(logicalData, referenceData, apiRequest.action);
};
const getChildEntitiesLogicalData = async (childEntities, parentNodeId, serviceOrchestrator) => {
    const logicalDatas = [];
    if (childEntities && childEntities.length > 0) {
        // will call graphql for getting all childnodes using parentNodeId
        let childNodes = null;
        //if (process.env.NODE_ENV === 'test') {
           // childNodes = JSON.parse(JSON.stringify(mockdata_1.childNodeMultiValued));
       // }
        //else {
            childNodes = await getChildNodes(parentNodeId);
       // }
        for (const child of childEntities) {
            let requiredNode = null;
            childNodes.some((node) => {
                if (node.configObjectId === child.nodeId) {
                    requiredNode = node;
                    return true;
                }
                return false;
            });
            if (requiredNode) {
                const dataList = await processRecords(requiredNode, child.records, serviceOrchestrator);
                let childLogicalData = [];
                const appData = new models_1.AppData(dataList);
                if (child.childEntities && child.childEntities.length > 0) {
                    childLogicalData = await getChildEntitiesLogicalData(child.childEntities, child.nodeId, serviceOrchestrator);
                }
                logicalDatas.push(new models_1.LogicalData(requiredNode, appData, childLogicalData));
            }
        }
    }
    return logicalDatas;
};
const sortPhysicalEntities = (physicalEntities) => {
    physicalEntities.sort((a, b) => {
        return a.order - b.order;
    });
};
const processRecords = async (requiredNode, records, serviceOrchestrator) => {
    const entity = Object.assign(new models_1.LogicalEntity(), requiredNode.entity);
    const dataList = [];
    sortPhysicalEntities(requiredNode.physicalDataEntities);
    let masterPhysicalEntity = requiredNode.physicalDataEntities[0];
    if (requiredNode.physicalDataEntities && requiredNode.physicalDataEntities.length > 1) {
        const primaryPE = requiredNode.physicalDataEntities.find((pe) => pe.isPrimaryEntity);
        if (primaryPE) {
            masterPhysicalEntity = primaryPE;
        }
    }
    if (records) {
        const { jsonNameWithDBCodeMap, dBCodeWithDatatypeMap } = entity.getPropertyMap();
        for (const record of records) {
            const data = await getDataMap(record, jsonNameWithDBCodeMap, dBCodeWithDatatypeMap);
            let oldRecord = {};
            try {
                oldRecord = await serviceOrchestrator
                    .selectSingleRecord(masterPhysicalEntity, data);
            }
            catch (error) {
                throw error;
            }
            Object.keys(oldRecord).length > 0 ? data[constant_1.EXECUTION_MODE] = constant_1.OperationType.UPDATE :
                data[constant_1.EXECUTION_MODE] = constant_1.OperationType.INSERT;
            dataList.push(data);
        }
    }
    return dataList;
};
const getDataMap = async (attributes, jsonNameWithDBCodeMap, dBCodeWithDatatypeMap) => {
    const data = {};
    for (const [dbCode, value] of Object.entries(attributes)) {
        const updatedValue = getFormattedValue(value, dBCodeWithDatatypeMap[dbCode], dbCode);
        if (jsonNameWithDBCodeMap[dbCode] !== undefined) {
            data[jsonNameWithDBCodeMap[dbCode]] = updatedValue;
        }
        else {
            data[dbCode] = updatedValue;
        }
    }
    return data;
};
const auditDates = ['CREATIONDATE', 'UPDATIONDATE', 'DELETIONDATE', 'APP_CURRENT_DATE'];
const getFormattedValue = (value, dataType, dbCode) => {
    switch (dataType) {
        case constant_1.DataType.TIMESTAMP:
            if (value !== null && !(value instanceof Date)) {
                return new Date(value).toISOString();
            }
            break;
        case constant_1.DataType.DATE:
            if (value !== null && !(value instanceof Date)) {
                return new Date(value).toISOString();
            }
            break;
        default:
            if (auditDates.includes(dbCode) && value !== null && !(value instanceof Date)) {
                return new Date(value).toISOString();
            }
    }
    return value;
};
const getParentNode = async (nodeId) => {
    try {
        if (info_commons_1.APP_ENV === 'desktop') {
            let query = metadata_utils_1.manipulateQuery(config_data_structure_1.GET_PARENT_NODE);
            query = query.replace("$id", '"' + nodeId + '"');
            const { data } = await appeng_meta_1.default(query);
            return data.CompositeEntityNode;
        }
        else {
            const { data } = await client.query({
                query: config_data_structure_1.GET_PARENT_NODE,
                variables: { id: nodeId },
            });
            return data.CompositeEntityNode;
        }
    }
    catch (error) {
        throw error;
    }
};
const getChildNodes = async (parentNodeId) => {
    try {
        if (info_commons_1.APP_ENV === 'desktop') {
            let query = metadata_utils_1.manipulateQuery(config_data_structure_1.GET_CHILD_NODES);
            query = query.replace("$id", '"' + parentNodeId + '"');
            const { data } = await appeng_meta_1.default(query);
            return data.ChildCompositeEntityNodes;
        }
        else {
            const { data } = await client.query({
                query: config_data_structure_1.GET_CHILD_NODES,
                variables: { id: parentNodeId },
            });
            return data.ChildCompositeEntityNodes;
        }
    }
    catch (error) {
        throw error;
    }
};
//# sourceMappingURL=transform.to.logical.data.js.map