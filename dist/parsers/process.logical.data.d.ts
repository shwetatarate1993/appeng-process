import { ServiceOrchestrator } from 'appeng-db';
import { EntityGroupData } from '../models';
export declare const processLogicalData: (entityGroupData: EntityGroupData, serviceOrchestrator: ServiceOrchestrator) => Promise<EntityGroupData>;
//# sourceMappingURL=process.logical.data.d.ts.map