import { EntityColumn } from '../models';
export default class DataSetRelColumns {
    parentColumn: EntityColumn;
    childColumn: EntityColumn;
}
//# sourceMappingURL=datasetrelcolumns.model.d.ts.map