import { AppData, CompositeEntityNode } from './';
export default class LogicalData {
    readonly appData: AppData;
    readonly ceNode: CompositeEntityNode;
    readonly childLogicalData: LogicalData[];
    constructor(node: CompositeEntityNode, appData: AppData, childData: LogicalData[]);
}
//# sourceMappingURL=logical.data.model.d.ts.map