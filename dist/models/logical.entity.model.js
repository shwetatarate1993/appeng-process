"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LogicalEntity {
    constructor() {
        this.getPropertyMap = () => {
            const jsonNameWithDBCodeMap = {};
            const dBCodeWithDatatypeMap = {};
            this.logicalColumns.map((col) => {
                if (col.jsonName !== null && col.jsonName !== '') {
                    jsonNameWithDBCodeMap[col.jsonName] = col.dbCode;
                }
                dBCodeWithDatatypeMap[col.dbCode] = col.dataType;
            });
            return { jsonNameWithDBCodeMap, dBCodeWithDatatypeMap };
        };
    }
}
exports.default = LogicalEntity;
//# sourceMappingURL=logical.entity.model.js.map