import { OperationType } from '../constant';
import { PhysicalEntity } from './';
export default class PhysicalData {
    readonly dbOperationType: OperationType;
    readonly physicalEntity: PhysicalEntity;
    readonly newData: any;
    constructor(dbOperationType: OperationType, physicalEntity: PhysicalEntity, newData: any);
}
//# sourceMappingURL=physical.data.model.d.ts.map