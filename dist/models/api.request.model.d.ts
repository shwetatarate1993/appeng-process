import { RequestMethod } from '../constant';
import { APIRequestEntity } from './';
export default class APIRequest {
    readonly action: string;
    readonly method: RequestMethod;
    readonly baseEntity: APIRequestEntity;
}
//# sourceMappingURL=api.request.model.d.ts.map