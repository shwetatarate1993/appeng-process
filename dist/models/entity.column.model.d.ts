import { DatabaseValidation, StandardValidation } from '.';
import { Mode } from '../constant';
export default class EntityColumn {
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly dataType: string;
    readonly length: number;
    readonly dbCode: string;
    readonly dbtypeName: string;
    readonly isPrimaryKey: boolean;
    readonly isVirtual: boolean;
    readonly isKey: boolean;
    readonly isPhysicalColumnMandatory: boolean;
    readonly isVerticalField: boolean;
    readonly isUnique: boolean;
    readonly isMandatory: boolean;
    readonly jsonName: string;
    readonly isDerived: boolean;
    readonly sourceColumn: EntityColumn;
    readonly mode: Mode;
    readonly databaseValidations: DatabaseValidation[];
    readonly standardValidations: StandardValidation[];
}
//# sourceMappingURL=entity.column.model.d.ts.map