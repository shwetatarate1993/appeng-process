"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PhysicalData {
    constructor(dbOperationType, physicalEntity, newData) {
        this.dbOperationType = dbOperationType;
        this.physicalEntity = physicalEntity;
        this.newData = newData;
        Object.freeze(this);
    }
}
exports.default = PhysicalData;
//# sourceMappingURL=physical.data.model.js.map