"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EntityGroupData {
    constructor(logicalData, referenceData, action) {
        this.logicalData = logicalData;
        this.referenceData = referenceData;
        this.action = action;
        Object.freeze(this);
    }
}
exports.default = EntityGroupData;
//# sourceMappingURL=entity.group.data.model.js.map