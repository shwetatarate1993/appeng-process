export default class AppData {
    readonly isProcessingDone: boolean;
    readonly derivedData: any;
    readonly data: any[];
    readonly oldData: any[];
    readonly processedData: any[];
    constructor(data: any[]);
}
//# sourceMappingURL=app.data.model.d.ts.map