export declare const DB: string;
export declare const LINKS: string;
export declare enum RequestMethod {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    DELETE = "DELETE"
}
export declare enum OperationType {
    INSERT = "INSERT",
    UPDATE = "UPDATE",
    DELETE = "DELETE"
}
export declare enum UniqueKey {
    UUID = "UUID",
    SQL = "SQL"
}
export declare enum Separator {
    Separator = "sEpArAtOr",
    ListSeparator = "lIsTSePaRaToR",
    Colon = ":"
}
export declare enum DataType {
    TIMESTAMP = "TIMESTAMP",
    DATE = "DATE"
}
export declare enum Mode {
    INSERT = "INSERT",
    UPDATE = "UPDATE",
    BOTH = "BOTH"
}
export declare enum ConfigType {
    FORMFIELD = "FormField",
    FORM = "Form"
}
export declare const EXECUTION_MODE: string;
export declare const CHILD_ENTITY_PREFIX: string;
//# sourceMappingURL=appeng.constant.d.ts.map