"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DB = 'db';
exports.LINKS = 'links';
var RequestMethod;
(function (RequestMethod) {
    RequestMethod["GET"] = "GET";
    RequestMethod["POST"] = "POST";
    RequestMethod["PUT"] = "PUT";
    RequestMethod["DELETE"] = "DELETE";
})(RequestMethod = exports.RequestMethod || (exports.RequestMethod = {}));
var OperationType;
(function (OperationType) {
    OperationType["INSERT"] = "INSERT";
    OperationType["UPDATE"] = "UPDATE";
    OperationType["DELETE"] = "DELETE";
})(OperationType = exports.OperationType || (exports.OperationType = {}));
var UniqueKey;
(function (UniqueKey) {
    UniqueKey["UUID"] = "UUID";
    UniqueKey["SQL"] = "SQL";
})(UniqueKey = exports.UniqueKey || (exports.UniqueKey = {}));
var Separator;
(function (Separator) {
    Separator["Separator"] = "sEpArAtOr";
    Separator["ListSeparator"] = "lIsTSePaRaToR";
    Separator["Colon"] = ":";
})(Separator = exports.Separator || (exports.Separator = {}));
var DataType;
(function (DataType) {
    DataType["TIMESTAMP"] = "TIMESTAMP";
    DataType["DATE"] = "DATE";
})(DataType = exports.DataType || (exports.DataType = {}));
var Mode;
(function (Mode) {
    Mode["INSERT"] = "INSERT";
    Mode["UPDATE"] = "UPDATE";
    Mode["BOTH"] = "BOTH";
})(Mode = exports.Mode || (exports.Mode = {}));
var ConfigType;
(function (ConfigType) {
    ConfigType["FORMFIELD"] = "FormField";
    ConfigType["FORM"] = "Form";
})(ConfigType = exports.ConfigType || (exports.ConfigType = {}));
exports.EXECUTION_MODE = 'b786d7ed-9644-11e9-ad17-12d8bca32300';
exports.CHILD_ENTITY_PREFIX = 'AppEngChildEntity:';
//# sourceMappingURL=appeng.constant.js.map